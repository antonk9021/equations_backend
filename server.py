# -*- coding: utf-8 -*-
"""
Application entry point. Serves simple HTTP server on the given host and port.
Example:
    python server.py
"""

from http.server import HTTPServer, SimpleHTTPRequestHandler
import json
import linsolve
from definitions import *
import util


class EquationsHandler(SimpleHTTPRequestHandler):
    def _set_headers(self, status_code, content_type):
        self.send_response(status_code)
        self.send_header("Content-Type", content_type)
        self.end_headers()

    @staticmethod
    def _serialize(content):
        jsonified = json.dumps(content, ensure_ascii=False)
        return bytes(jsonified, encoding="utf-8")

    def do_GET(self):
        if self.path == '/':
            template = util.get_template()
            self._set_headers(200, CONTENT_TYPE_GET)
            self.wfile.write(bytes(template.render(results=None), "utf-8"))
        else:
            return SimpleHTTPRequestHandler.do_GET(self)

    def do_POST(self):
        content_length = int(self.headers.get("Content-Length"))
        raw_body = self.rfile.read(content_length)
        body = json.loads(raw_body)
        try:
            solution = list(linsolve.calculate_pairwise(**body))
            status_code = 200
        except ZeroDivisionError:
            status_code = 500
            solution = {"error": "The system is either dependent or inconsistent."}
        self._set_headers(status_code, CONTENT_TYPE_POST)
        self.wfile.write(self._serialize(solution))


def run(server_class=HTTPServer, handler_class=EquationsHandler, addr=HOST, port=PORT):
    server_address = (addr, port)
    httpd = server_class(server_address, handler_class)

    print(f"Starting equations server on {addr}:{port}...")
    httpd.serve_forever()


if __name__ == '__main__':
    run()
