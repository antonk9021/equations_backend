# -*- coding: utf-8 -*-
"""
Utility functions to be used throughout the project.
"""

from typing import List, Union, Iterator, Any, Iterable, Tuple, Dict
from jinja2 import Environment, PackageLoader, select_autoescape

Numeric = Union[int, float, complex]


def deepcopy(list_: List[Any]) -> List[Any]:
    """
    Performs deep copy of the input list including its inner lists.
    :param list_: Input list.
    """
    return [x[:] for x in list_]


def complex_round(number: Numeric, precision: int) -> Numeric:
    """
    round() which supports complex numbers.
    :param number: Input number.
    :param precision: Desired precision.
    """
    if isinstance(number, complex):
        return round(number.real, precision) + round(number.imag, precision)*1j
    return truncate_zero(round(number, precision))


def truncate_zero(number: Numeric) -> Numeric:
    """
    Truncates unnecessary zeros for more convenient representation.
    For example: 5.0 -> 5; 100.0 -> 100
    :param number: Number to be truncated.
    """
    if float(number) != int(number):  # TODO: Make support for complex numbers
        return number
    return int(number)


def truncate_numbers_zeros(numbers_dict: Dict[str, Numeric]) -> Dict[str, Numeric]:
    """
    Truncates all zeros from input dict values.
    :param numbers_dict: Input dict, which values are numbers.
    """
    return {k: truncate_zero(v) for k, v in numbers_dict.items()}


def twist_zip(iter_1: List[Numeric], iter_2: List[Numeric]) -> List[Dict[str, Numeric]]:
    """
    Zips two iterables after reverting one of them.
    # TODO: Add example.
    :param iter_1: Untouched iterable.
    :param iter_2: Iterable to be reversed.
    """
    for item_1, item_2 in zip(iter_1, iter_2):
        index_1 = iter_1.index(item_1)
        index_2 = iter_2.index(item_2)
        temp = []
        if iter_1.index(item_1) % 2:
            temp.append(iter_1.pop(index_1))
            iter_2.append(temp.pop())
            temp.append(iter_2.pop(index_2))
            iter_1.append(temp.pop())
    return [dict(zip(['x_1', 'x_2'], iter_1)), dict(zip(['x_1', 'x_2'], iter_2))]


def set_template_x_2_sign(equation: Dict[str, Numeric]) -> str:
    """
    Sets sign for the x_2 which will be represented within resulting .html
    :param equation: Equation which x_2 sign should be set.
    """
    if equation["x_2"] < 0:
        return '-'
    elif any([x == 0 for x in [equation["x_1"], equation["x_2"]]]):
        return ''
    else:
        return '+'


def get_template():
    """
    Retrieves empty index.html template.
    """
    env = Environment(
        loader=PackageLoader(__name__, "templates"),
        autoescape=select_autoescape(["html"])
    )
    env.globals["set_sign"] = set_template_x_2_sign  # Registering this function for the Jinja2 template

    return env.get_template("index.html")


def render_template(func):
    """
    A decorator which transforms dict or JSON into a Jinja2 template.
    """
    template = get_template()

    def wrapper(**kwargs):
        results = func(**kwargs)
        return template.render(results=results)
    return wrapper
