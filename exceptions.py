class MetaException(type):
    def __new__(cls, *args, **kwargs):
        exception = super().__new__(cls, *args, **kwargs)
        #exception.message = kwargs["message"]
        return exception


# class NotASquareMatrix(bases=(BaseException,), metaclass=MetaException):
#     pass

#NotASquareMatrix = type("NotASquareMatrix", (BaseException,), {"message": "Matrix should have square shape."})

class NotASquareMatrix(BaseException):
    """
    Raised when the matrix should be square but it isn`t.
    """

    def __init__(self):
        self.message = "Matrix should have square shape."
        super().__init__(self.message)
