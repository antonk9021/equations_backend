# An equations solver: back-end part

This is readme for Python-based solver for systems of linear equations

## Requirements
- Python 3.6+
- Packages from *requirements.txt*

## Installation and running
- Create virtual environment if desired
  - Activate the environment 
- Install required packages with `pip install -r requirements.txt`
- Enter the working directory where the project resides
- Launch with `python server.py`