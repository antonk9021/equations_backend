import os
import multiprocessing
from dotenv import load_dotenv

if not os.path.exists(".env"):
    raise FileNotFoundError(".env file not found.")
load_dotenv(".env")

CPU_COUNT = multiprocessing.cpu_count()
NONZERO = 1e-18
CSV_FILE = os.getenv("CSV_FILE")
# FUNC = [3, 1]
HOST = os.getenv("HOST")
PORT = int(os.getenv("PORT"))
CONTENT_TYPE_GET = "text/html"
CONTENT_TYPE_POST = "application/json"
