import functools
import util
from definitions import *
from exceptions import *
from typing import List, Union, Iterator, Any


Numeric = Union[int, float, complex]


def determinant(matrix: List[List[Numeric]]) -> Numeric:
    # Matrix should be square:
    if len(matrix) != len(matrix[0]):
        raise NotASquareMatrix

    # if filter # TODO: Complete rows equality check

    # If matrix contains the only element, this element represents itself its determinant:
    if len(matrix) == 1:
        return matrix[0][0]

    n_rows = len(matrix)

    matrix_copy = util.deepcopy(matrix)

    for diag in range(n_rows):
        for i in range(diag+1, n_rows):
            if matrix_copy[diag][diag] == 0:
                #return 0
                matrix_copy[diag][diag] = NONZERO
            scaler = matrix_copy[i][diag] / matrix_copy[diag][diag]
            for j in range(n_rows):
                matrix_copy[i][j] = matrix_copy[i][j] - scaler*matrix_copy[diag][j]
    result = functools.reduce(lambda a, b: a*b, [matrix_copy[i][i] for i in range(n_rows)])
    return util.complex_round(result, 3)


def solve_cramer(eq_matrix: List[List[Numeric]], results: List[Numeric]) -> Iterator[Numeric]:
    det = determinant(eq_matrix)
    if det == 0:
        raise ZeroDivisionError
    for j, column in enumerate(eq_matrix[0]):
        current_matrix = util.deepcopy(eq_matrix)
        for row, result in zip(current_matrix, results):
            row[j] = result
        yield util.complex_round(determinant(current_matrix)/det, 3)
