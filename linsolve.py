import models
import util
import csv
import itertools
from definitions import *
from typing import Iterable, Tuple, Iterator, Any, Dict, List, Union


Numeric = Union[int, float, complex]

# def frac(dec):
#     sign = "-" if dec < 0 else ""
#     frac = Fraction(abs(dec))
#     return (f"{sign}{frac.numerator // frac.denominator} "
#             f"{frac.numerator % frac.denominator}/{frac.denominator}")


# def read_matrix_file(filename: str) -> Iterator[Dict[str, str]]:
#     with open(filename, 'r', encoding="utf-8") as f:
#         matrix = csv.DictReader(f, delimiter=',')
#
#         for row in matrix:
#             yield {"vars": [float(v) for k, v in row.items() if k.lower() != 'b'], 'B': float(row['B'])}

#@util.render_template
def calculate_pairwise(obj_func: List[Numeric], equations: List[Dict[str, Any]]) -> Iterator[Dict[str, Iterable]]:
    """
    Solves each pair of linear equations from the given set.
    :param obj_func: Objective function quotients.
    :param equations: Dict of equations quotients and free terms.
    """
    # For each pair of equations within the given set:
    for eq_1, eq_2 in itertools.combinations(equations, 2):
        zeros_1 = [util.complex_round(eq_1['B']/var, 3) if var != 0 else None for var in eq_1["vars"]]
        zeros_2 = [util.complex_round(eq_2['B']/var, 3) if var != 0 else None for var in eq_2["vars"]]
        intersection = list(models.solve_cramer([eq_1["vars"], eq_2["vars"]], [result['B'] for result in [eq_1, eq_2]]))
        func_max = sum(a*b for a, b in zip(intersection, obj_func))
        yield {
            "eq_1": util.truncate_numbers_zeros({
                "x_1": eq_1["vars"][0],
                "x_2": eq_1["vars"][1],
                "B": eq_1['B']
            }),
            "eq_2": util.truncate_numbers_zeros({
                "x_1": eq_2["vars"][0],
                "x_2": eq_2["vars"][1],
                "B": eq_2['B']
            }),
            "intersection": dict(zip(['x_1', 'x_2'], intersection)),
            "func_value": util.complex_round(func_max, 3),
            "zeros": {
                "eq_1": util.twist_zip(zeros_1, [0, 0]),
                "eq_2": util.twist_zip(zeros_2, [0, 0])
            }
        }
