var TIMEOUT = 4001;
var eqCounter = 3;
var fadeOutTimer;

/*
const observer = new MutationObserver(function(mutations) {
    for (let mutation of mutations) {
        if (mutation.target.textContent === '') {
            alert(wrongResults.error);
        }
    }
});
*/

/** Header shrinking and top arrow-button behaviour on scrolling */
window.onscroll = function() {
    let toTopButton = document.getElementById("back-to-top");
    let head = document.getElementById('head');
    let funcButtons = document.getElementsByClassName('functional-button')
    toTopButton.style.display = (document.body.scrollTop > 300 || document.documentElement.scrollTop > 300) ? "block" : "none";
    head.style.fontSize = (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) ? 'small' : 'medium';
    for (let button of funcButtons) {
        button.style.padding = (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) ? '.5%' : '1.5%';
        button.style.fontSize = (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) ? '1vw' : '1.5vw';
    }
}


function backToTop() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}


function addEquation() {
    let eqCounterStr = eqCounter.toString();
    
    let removeButton = document.getElementById('remove-equation');    

    /** Creating the container div for the equation */
    let newEquationDiv = document.createElement('div');

    /** Creating the new equation label */
    let equationLabel = document.createElement('label');
    equationLabel.innerHTML = `Equation No. ${eqCounterStr}: `

    /** Creating input fields with respective labels */
    let [x_1, x_1_label] = createEquationVariable('1');
    let [x_2, x_2_label] = createEquationVariable('2');
    let [B, B_label] = createEquationVariable('B');
    let signs = createSignsSwitcher(eqCounterStr);

    /** Appending all fields and labels to the parent div */
    for (let child of [equationLabel, x_1, x_1_label, signs, x_2, x_2_label, B_label, B]) {
        newEquationDiv.appendChild(child);
        newEquationDiv.appendChild(document.createTextNode(' '));
    }

    let insertionPoint = document.getElementById('next-equation');
    newEquationDiv.className = 'equation-input';

    let parent = insertionPoint.parentNode;
    parent.insertBefore(newEquationDiv, insertionPoint);

    eqCounter++;

    if (eqCounter > 2) {
        removeButton.disabled = false;
        removeButton.classList.remove('remove-equation-disabled');
        removeButton.classList.add('remove-equation-enabled');
    }
}


function createEquationVariable(varName) {
    let eqCounterStr = eqCounter.toString();
    let varInput = document.createElement('' + 'input');
    let labelText = varName === 'B' ? 'B': `x_${varName}`;
    varInput.setAttribute('type', 'text');
    varInput.id = `eq_${eqCounterStr}_${labelText}`;

    let varLabel = document.createElement('label');
    varLabel.setAttribute('for', `eq_${eqCounterStr}_${labelText}`);
    varLabel.innerHTML = varName === 'B' ? '=': `x<sub>${varName}</sub>`;

    return [varInput, varLabel];
}


function createSignsSwitcher(eqNumber) {
    let select = document.createElement('select');
    select.setAttribute('name', `eq_${eqNumber}_sign`);

    let optionPlus = createSignOption('+');
    let optionMinus = createSignOption('-');

    select.appendChild(optionPlus);
    select.appendChild(optionMinus);

    return select;
}

function createSignOption(sign) {
    let option = document.createElement('option');
    option.setAttribute('value', sign);
    option.innerHTML = sign;

    if (sign === '+') {
        option.setAttribute('default', true);
    }

    return option;
}


function removeEquation() {
    let removeButton = document.getElementById('remove-equation');
    if (eqCounter > 3) {
        let allEquations = document.getElementsByClassName('equation-input');
        let lastEq = allEquations[allEquations.length-1];
        lastEq.remove();
    
        eqCounter--;

        if (eqCounter <= 3) {
            removeButton.disabled = true;
            removeButton.classList.remove('remove-equation-enabled'); 
            removeButton.classList.add('remove-equation-disabled');
        }
    }
}


function solve() {
    /** Retrieving objective function quotients */
    let obj_func = [+document.getElementById('a1').value, +document.getElementById('a2').value];

    /** Creating array with equations variables */
    let equations = [];

    /** Both `obj_func` and `equations` arrays will be POSTed to the server for the calculation */
    
    /** Populating the array with the data from the input fields */
    for (let i=1; i<eqCounter; i++) {
        /** x_1 value */
        let x_1 = +document.getElementById(`eq_${i}_x_1`).value;

        /** If the sign before x_2 is `-`, -x_2 will be appended, otherwise just x_2 */
        let sign = document.querySelector(`select[name=eq_${i}_sign]`).value === '-' ? -1 : 1;
        let x_2 = sign*(+document.getElementById(`eq_${i}_x_2`).value);
        /** Free factor value */
        let B = +document.getElementById(`eq_${i}_B`).value;

        equations.push({'vars': [x_1, x_2], 'B': B});
    }
 
    /** Serializing the array */
    let body = JSON.stringify({'obj_func': obj_func, 'equations': equations});

    let request = new Request('/', {method: 'POST', headers: {'Content-Type': 'application/json'}, body: body})
    fetch(request)
    .then(function(response) {
        if (response.ok) {
            response.json().then(function(results) {
                displayResults(results);
            })
        } else {
            response.json().then(function(results) {
                alertInconsistentOrDependent(results);
            })
        }
    })
    .catch(function(error) {
        console.log(error);
    });
}


function displayResults(results) {
    let optimalObjFuncButton = document.getElementById('highlight-optimum-disabled');
    optimalObjFuncButton.classList.remove('highlight-optimum-disabled');
    optimalObjFuncButton.classList.add('highlight-optimum-enabled');
    optimalObjFuncButton.disabled = false;

    let resultsDiv = document.getElementById('results');

    if (resultsDiv !== null) {
        resultsDiv.innerHTML = '';
    } else {
        resultsDiv = document.createElement('div');
        resultsDiv.id = 'results';
        resultsDiv.classList.add('results');
        resultsDiv.classList.add('functional-block');

        let insertionPoint = document.getElementById('results-insertion-point');
        let parent = insertionPoint.parentNode;
        parent.insertBefore(resultsDiv, insertionPoint);
    }    

    let resultsTitle = document.createElement('h2');
    resultsTitle.innerHTML = 'Calculation results';

    let allEquations = document.getElementsByClassName('equation-input');
    let resultsCaption = document.createElement('h3');

    /** Note about yield of equations pairs calculated through C(number of all equations, 2) */
    resultsCaption.innerHTML = `Total C(${allEquations.length}, 2) = ${results.length} pair${allEquations.length > 2 ? 's' : ''} of equations.`;

    let resultsHeader = document.createElement('div');
    resultsHeader.className = 'results-header';

    resultsHeader.appendChild(resultsTitle);
    resultsHeader.appendChild(resultsCaption);
    resultsDiv.appendChild(resultsHeader);

    for(let i=0; i < results.length; i++) {
        let pairHeader = document.createElement('h3');
        pairHeader.innerHTML = `Equations pair No. ${i+1}`;
        let result = results[i];

        let eqOneXOne = result.eq_1.x_1;
        let eqOneXTwo = result.eq_1.x_2;
        let eqOneB = result.eq_1.B;

        let eqTwoXOne = result.eq_2.x_1;
        let eqTwoXTwo = result.eq_2.x_2;
        let eqTwoB = result.eq_2.B;

        let eqPair = document.createElement('div');
        eqPair.className = 'pair-solved';
        eqPair.id = `eq-solved-${i+1}`;

        let eqOne = solvedEquation(eqOneXOne, eqOneXTwo, eqOneB);
        let eqTwo = solvedEquation(eqTwoXOne, eqTwoXTwo, eqTwoB);

        let intersection = JSON.stringify(result.intersection);

        let intersectionCoords = document.createElement('p');
        let intersectionCoordsLabel = document.createElement('label');
        intersectionCoordsLabel.innerHTML = `Intersection point: ${intersection}`;
        intersectionCoords.appendChild(intersectionCoordsLabel);

        let zerosEq1 = document.createElement('p'); 
        zerosEq1.innerHTML = `Zeros for equation No. 1: ${JSON.stringify(result.zeros.eq_1)}`;

        let zerosEq2 = document.createElement('p');
        zerosEq2.innerHTML = `Zeros for equation No. 2: ${JSON.stringify(result.zeros.eq_2)}`;

        let funcValuePara = document.createElement('p');
        let funcValueLabel = document.createElement('label');
        funcValueLabel.innerHTML = 'Objective function value at the intersection point:';
        let funcValueActual = document.createElement('label');
        funcValueActual.innerHTML = `${result.func_value}`;
        funcValueActual.className = `obj-func-value`;
        for (let child of [funcValueLabel, document.createTextNode(' '), funcValueActual]) {
            funcValuePara.appendChild(child);
        }

        for (let child of [pairHeader, eqOne, eqTwo, intersectionCoords, zerosEq1, zerosEq2, funcValuePara]) {
            eqPair.appendChild(child);
        }

        resultsDiv.appendChild(eqPair);
    }
}


function alertInconsistentOrDependent(wrongResults) {
    let prevSolution = document.getElementById('results');

    if (prevSolution !== null) {
        prevSolution.innerHTML = '';
    }
    alert(wrongResults.error);

}


function solvedEquation(x_1, x_2, B) {
    let eq = document.createElement('div');
    eq.className = 'equation-solved';
    let xOneString = formatX(x_1, 1); 
    let xTwoString = formatX(Math.abs(x_2), 2);
    
    let sign;
    if ([x_1, x_2].includes(0)) {
        sign = '';
    } else {
        sign = ` ${x_2 < 0 ? '-' : '+'} `
    }
    eq.innerHTML = `${xOneString}${sign}${xTwoString} = ${B}`;

    return eq;
}


function formatX(xQuotient, varNum) {
    let xString;

    if (+xQuotient !== 0) {
        xString = `${xQuotient == 1 ? '' : xQuotient}x<sub>${varNum.toString()}</sub>`;
    } else {
        xString = '';
    }

    return xString;
}


function highlight(divId) {
    clearTimeout(fadeOutTimer);
    console.log("Timeout dropped");

    let div = document.getElementById(divId);
    div.scrollIntoView({block: "center", behavior: "smooth"});
    div.classList.remove('highlighted');
    div.classList.add('highlighted');

    //let removeHighlighted = div.classList.remove('highlighted')
    fadeOutTimer = setTimeout(function() {
        div.classList.remove('highlighted')
    }, TIMEOUT);
    console.log("Timeout set");
}


function highlightOptimalObjFunc() {
    let optimization = document.querySelector('.optimization').value;

    let optimalObjFunc = findOptimalObjFunc(optimization);
    highlight(optimalObjFunc.id);
}


function findOptimalObjFunc(optimization) {
    let allSolutions = document.getElementsByClassName('obj-func-value');
    let optFunc = {};
    optFunc.id = allSolutions[0].parentNode.parentNode.id;
    optFunc.value = +allSolutions[0].innerHTML;

    switch(optimization) {
        case 'max':
            for (let solution of allSolutions) {
                if (optFunc.value < +solution.innerHTML) {
                    optFunc.id = solution.parentNode.parentNode.id;
                    optFunc.value = +solution.innerHTML;
                }
            }
            break;
        case 'min':
            for (let solution of allSolutions) {
                if (optFunc.value > +solution.innerHTML) {
                    optFunc.id = solution.parentNode.parentNode.id;
                    optFunc.value = +solution.innerHTML;
                }
            }
            break;
    }

    return optFunc;
}

